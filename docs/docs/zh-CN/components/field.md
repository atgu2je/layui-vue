::: demo

<template>
  <lay-field title="标题">内容</lay-field>
</template>

<script>
import { ref } from 'vue'

export default {
  setup() {

    return {
    }
  }
}
</script>

:::

::: demo

<template>
  <lay-field title="标题">内容</lay-field>
</template>

<script>
import { ref } from 'vue'

export default {
  setup() {

    return {
    }
  }
}
</script>

:::

::: demo

<template>
  <lay-field title="标题"></lay-field>
</template>

<script>
import { ref } from 'vue'

export default {
  setup() {

    return {
    }
  }
}
</script>

:::

::: field field attributes

:::

|       |      |     |
| ----- | ---- | --- |
| title | 标题 | --  |

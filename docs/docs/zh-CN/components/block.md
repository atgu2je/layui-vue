::: demo

<template>
  <lay-block>引用区域的文字</lay-block>
</template>

<script>
import { ref } from 'vue'

export default {
  setup() {

    return {
    }
  }
}
</script>

:::

::: demo

<template>
  <lay-block nm>引用区域的文字</lay-block>
</template>

<script>
import { ref } from 'vue'

export default {
  setup() {

    return {
    }
  }
}
</script>

:::

::: field block attributes

:::

|       |        |                |
| ----- | ------ | -------------- |
| nm | 灰色样式 | -- |
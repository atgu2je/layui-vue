::: demo

<template>
  <lay-empty></lay-empty>
</template>

<script>
import { ref } from 'vue'

export default {
  setup() {

    return {
    }
  }
}
</script>

:::

::: demo

<template>
  <lay-empty description="刷新试试"></lay-empty>
</template>

<script>
import { ref } from 'vue'

export default {
  setup() {

    return {
    }
  }
}
</script>

:::

::: field empty attributes

:::

|             |          |     |
| ----------- | -------- | --- |
| description | 描述信息 | --  |

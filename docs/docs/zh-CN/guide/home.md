<img src="../../../src/assets/logo-2.png" style="height:220px;"/>

<lay-block>
layui - vue（谐音：类 UI) 是 一 套 Vue 3.0 的 桌 面 端 组 件 库 , Layui 的 另 一 种 呈 现 方 式
</lay-block>

<lay-timeline style="padding-left:38px;padding-top:30px;">
  <lay-timeline-item title="2021年，layui vue 里程碑版本 0.0.1 发布" simple></lay-timeline-item>
  <lay-timeline-item title="2017年，layui 里程碑版本 2.0 发布" simple></lay-timeline-item>
  <lay-timeline-item title="2016年，layui 首个版本发布" simple></lay-timeline-item>
  <lay-timeline-item title="2015年，layui 孵化" simple></lay-timeline-item>
</lay-timeline>

<div>
  <ul class="layui-row layui-col-space30 site-idea">
    <li class="layui-col-md4">
      <div>
        <fieldset class="layui-elem-field layui-field-title">
          <legend>返璞归真</legend>
          <p>身处在前端社区的繁荣之下，我们都在有意或无意地追逐。而 layui 偏偏回望当初，奔赴在返璞归真的漫漫征途，自信并勇敢着，追寻于原生态的书写指令，试图以最简单的方式诠释高效。</p>
        </fieldset>
      </div>
    </li>
    <li class="layui-col-md4">
      <div>
        <fieldset class="layui-elem-field layui-field-title">
          <legend>双面体验</legend>
          <p>拥有双面的不仅是人生，还有 layui。一面极简，一面丰盈。极简是视觉所见的外在，是开发所念的简易。丰盈是倾情雕琢的内在，是信手拈来的承诺。一切本应如此，简而全，双重体验。</p>
        </fieldset>
      </div>
    </li>
    <li class="layui-col-md4">
      <div>
        <fieldset class="layui-elem-field layui-field-title">
          <legend>星辰大海</legend>
          <p>如果眼下还是一团零星之火，那运筹帷幄之后，迎面东风，就是一场烈焰燎原吧，那必定会是一番尽情的燃烧。待，秋风萧瑟时，散作满天星辰，你看那四季轮回<!--海天相接-->，正是 layui 不灭的执念。</p>
        </fieldset>
      </div>
    </li>
  </ul>
</div>
